/* SPDX-License-Identifier: Zlib
   Copyright (C) 2022 smpl <smpl@slamkode.ml> */

let MastodonInteract = {
	sharetext: null,
	account: null,
	dialogs: {
		follow: null,
		share: null,
	},
	cbAction: function(e) {
		var input = e.target.querySelector('input');
		var handle = input.value;
		
		if(handle.charAt(0) == '@') {
			handle = handle.substring(1);
		}
		
		var parts = handle.split('@');
		if(parts.length < 2) {
			input.value = '';
			input.setAttribute('placeholder', "Invalid handle");
			return;
		}
		
		if(e.target === this.dialogs.follow) {
			window.location = "https://" + parts[1] + "/intent?uri=web%2Bmastodon%3A%2F%2Ffollow%3Furi%3Dacct%3A" + encodeURIComponent(this.account);
		} else if(e.target === this.dialogs.share) {
			if(this.sharetext === null)
				this.sharetext = window.location;
			window.location = "https://" + parts[1] + "/intent?uri=web%2Bmastodon%3A%2F%2Fshare%3Ftext%3D" + encodeURIComponent(this.sharetext);
		}
	},
	init: function(account) {
		var self = this;
		
		window.addEventListener("DOMContentLoaded", function(e) {
			self.dialogs.follow = document.getElementById('ma-flw-form');
			if(self.dialogs.follow) {
				self.dialogs.follow.onsubmit = function(e) { self.cbAction(e); e.preventDefault(); };
			}
			
			self.dialogs.share = document.getElementById('ma-sha-form');
			if(self.dialogs.share) {
				self.dialogs.share.onsubmit = function(e) { self.cbAction(e); e.preventDefault(); };
			}
		});
	}
};

MastodonInteract.init();
MastodonInteract.account = "smpl@norrebro.space";
